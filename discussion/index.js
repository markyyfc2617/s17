




// using assignment operator
numbers[4] = "five";
console.log(numbers);


// Push Method
	// add an element/s at the end of the array
number.push("element");
console.log(numbers)

// push method using callback function
/*// callback function - a function that is passed on to another function. this is done because the inserted
function is following a particular syntax and the developer is trying to simplify that syntax by just inserting it inside another function*/
function pushMethod(element){
	numbers.push(element);
}
pushMethod("six");
pushMethod("seven");
pushMethod("eight");
console.log(numbers);


// REMOVING OF AN ELEMENT
// pop method - removes the element at the end of the array (last element)
numbers.pop();
console.log(numbers);


function popMethod(){
	numbers.pop()
}
popMethod();
console.log(numbers);



// ==============
// Manipulating the beginning/start of the array
// remove the first element
// shift method - remove the first element of the array
numbers.shift();
console.log(numbers);


// callback function
function shiftMethod(){
	numbers.shift();
}
shiftMethod();
console.log(numbers);


// ===============
// ADDING AN ELEMENT
// unshift method
numbers.unshift("zero");
console.log(numbers);


// callback function
function unshiftMethod(element){
	numbers.unshift(element);
}
unshiftMethod("mcdo");
unshiftMethod(1);
console.log(numbers);


// Arrangement of the elements

let numbs = [15, 27, 32, 12, 6, 8, 236]
console.log(numbs);
// sort method - arranges the elements in ascending or descending. It has an anonymous function inside that has 2 parameteres
	/*
		2 parameters inside the anonymous function represents
			first parameter - start/smallest value element
			second parameter - last/biggest/ending element
	*/


/*
	SYNTAX
		arrayName.sort(
			function (a-b){
				a - b ascending
				b - a descending
			}

		)

*/

numbs.sort(
	function(a, b){
		return a - b
	}
);
console.log(numbs);



//descending order
numbs.sort(
	function(a, b){
		return b - a
	}
);
console.log(numbs);



// reversed method - reverses the order of the elements in an array no matter their arrangement
// it will depend on the last arrangement of the array, regardless if it is ascending, descending, descending have in random order
numbs.reverse();
console.log(numbs);


// ==================
// splice method
/*
	- directly manipulates the array
	- first parameter - the index of the element from which the omitting will begin
	- second parameter - determines the number of elements to be ommitted
	- third parameter - the replacements for the removed elements

*/
// one parameter - (pure omission)
// let nums = numbs.splice(1);
// two parameter - (pure omission)
// let nums = numbs.splice(1,2);
// three parameters (replacement)
let nums = numbs.splice(4, 2, 31, 11);
console.log(numbs);
console.log(nums);



// =================
// slice method - ctrl-c + ctrl v

/*
	- does not affect the original array; creates a sub-array, but does not omit any element from the original array

	-first parameter - index where copying will begin
	-second parameter - the number of elements to be copied starting from the first element (copyning will still begin at the first paramenter)

*/
// one parameter
// let sliceNums = numbs.slice(1);
// two parameter - copy from 4th index up to 6th element
let sliceNums = numbs.slice(4,6);
console.log(numbs);
console.log(sliceNums);


// ===============
// merging array
// concat
console.log(numbers);
console.log(numbs);
let animal = ["dog", "tiger", "kangaroo", "chicken"];
console.log(animals);


let newConcat = numbers.concat(numbs, animals);
console.log(newConcat);
console.log(numbers);
console.log(numbs);
console.log(animals);



// join method - merges the elements inside the array and makes them string data
let meal = ["rice", "steak", "juice"];
console.log(meal);

let newJoin = meal.join();
console.log(newJoin);

newJoin = meal.join("");
console.log(newJoin);


newJoin = meal.join(" ");
console.log(newJoin);


newJoin = meal.join("-");
console.log(newJoin);


// toString Method
console.log(nums);
// typeof determines the data type of the element after it
console.log(typeof nums);

let newString = numbs.toString();
console.log(typeof newString)


/*Accessors*/
let countries = ["US", "PH", "JP", "HK", "SG", "PH" "NZ"];
// indexOf - returns the first index it finds from the beginning of the array

let index = countries.indexOf("PH");
console.log(index);

// finding a non existing element
index = countries.indexOf("AU");
console.log(index);

// lastIndexOf() - finds the index of the element starting from the end of the array
index = countries.lastIndexOf("PH");
console.log(index);

// returns -1 for non existing index
index = countries.lastIndexOf("Ph");
console.log(index);



/*if (countries.indexOf("PH") === -1){
	console.log("Element not existing");
}
else{
	console.log("Element exists in the array")
}*/


// iterators
// forEach

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
console.log(days);


// forEach - returns (performs the statement in) each element in the array
days.forEach(
	function(element){
		console.log(element)
	}

	)


// map
	// returns a copy of an array from the original which can be manipulated
let mapDays = days.map(
	function(element){
		return `${element} is the day of the week`
	}

)
console.log(mapDays);
console.log(days);



// filter - filters the elements and copies them into another array
console.log (numbs);
let newFilter = numbs.filter(
		function (element) {
			return element < 30;
		}
)
console.log(newFilter);



// includes - returns true if the elements are inside the array
let animalIncludes = animals.includes("dog");
console.log(animalIncludes);


// every - checks if all the element pass the condition. (returns true if all of them do)

let newEvery = nums.every(
	function (element){
		return (element > 10);
	}

)
console.log(newEvery);


// some - checks if atleast 1 elements passes the condition
let newSome = nums.some(
	function (element){
		return (element > 30);
	}


)
console.log(newSome);


nums.push(50)
console.log(nums);
// reduce - performs the operations in all of the elements in the array
// first parameter - first element
// second parameyer - last element
let newReduce = nums.reduce(
	function (a, b){
		return a + b;
	}

)
console.log(newReduce);



let average = newReduce/nums.length
console.log(average);


// toFixed - to sets the number of decimal places
console.log(average.toFixed(2));

/*
	parseInt - rounds the number to the nearest whole number
	parseFloat - rounds the number to the nearest target decimal place (through the use of .toFixed)

*/
console.log(parseInt(average.toFixed(2)));
console.log(parseFloat(average.toFixed(2)));